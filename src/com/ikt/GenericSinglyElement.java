package com.ikt;

public class GenericSinglyElement<T> {

    protected T value;
    protected GenericSinglyElement successor;

    public GenericSinglyElement(T value) {
        this.value = value;
        this.successor = null;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public GenericSinglyElement getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(GenericSinglyElement successor) {
        this.successor = successor;
    }

    public boolean hasNext() {
        return this.getSuccessor() != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericSinglyElement element = (GenericSinglyElement) o;
        return this.value == element.value;
    }

}
