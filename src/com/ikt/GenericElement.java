package com.ikt;

public class GenericElement<T> {


    protected T value;
    protected GenericElement predecessor;
    protected GenericElement successor;

    public GenericElement(T value) {
        this.value = value;
        this.successor = null;
        this.predecessor = null;
    }

    public T getValue() {
        return this.value;
    }

    public GenericElement getSuccessor() {
        return this.successor;
    }

    public GenericElement getPredecessor() {
        return this.predecessor;
    }

    public void setPredecessor(GenericElement predecessor) {
        this.predecessor = predecessor;
    }

    public void setSuccessor(GenericElement successor) {
        this.successor = successor;
    }


    public boolean hasNext() {
        return this.getSuccessor() != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericElement element = (GenericElement) o;
        return this.value == element.value;
    }

}
