package com.ikt;

import de.htwberlin.fiw.profiler.ProfiledClass;
import de.htwberlin.fiw.profiler.Profiler;

import java.util.Random;

public class Main extends ProfiledClass {

    public void run() {

        GenericsDoublyLinkedList genericsDoublyLinkedList = new GenericsDoublyLinkedList();


        for (int i = 0; i < 10000; i++) {
            GenericElement element = new GenericElement(new Random().nextLong());
            genericsDoublyLinkedList.append(element);

        }
        genericsDoublyLinkedList.print();
        genericsDoublyLinkedList.printReverse();
        GenericSinglyLinkedList genericSinglyLinkedList = new GenericSinglyLinkedList();

        for (int i = 0; i < 10000; i++) {
            GenericSinglyElement element = new GenericSinglyElement(new Random().nextLong());
            genericSinglyLinkedList.prepend(element);

        }
        genericSinglyLinkedList.print();
        genericSinglyLinkedList.printReverse();
        GenericsSet genericsSet = new GenericsSet();
        for (int i = 0; i < 10000; i++) {
            GenericElement element = new GenericElement(new Random().nextLong());
            genericsSet.add(element);

        }
        genericsSet.print();
        genericsSet.printReverse();

    }

    public static void main(String[] args) {

        Profiler profiler = new Profiler(Main.class);
        profiler.start();
        profiler.printResults();

    }

}

